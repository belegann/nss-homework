# Název Aplikace

## Seznam Autorů
- ANNA BELEGOVA
- JIŘÍ ŠEBEK

## Seznam Použitých Technologií
- Java 
- Spring Boot
- React
- PostgreSQL

## Popis
Toto je šestý domácí úkol pro NSS.


## Instalace
1. Klonujte tento repozitář na svůj počítač.
2. Nainstalujte potřebné závislosti pomocí správce balíčků (např. npm pro frontend a Maven pro backend).
3. Nakonfigurujte připojení k databázi v konfiguračním souboru.
4. Spusťte aplikaci pomocí příkazu `npm start` pro frontend a `mvn spring-boot:run` pro backend.

## Copyright
© 2024 Všechna práva vyhrazena.